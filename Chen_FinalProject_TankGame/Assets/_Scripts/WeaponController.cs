﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Complete;

public class WeaponController : MonoBehaviour {

    public GameObject shot;
    public Transform shotSpawn;
    public float fireRate;
    public float delay;

    private AudioSource audioSource;
    Complete.TankShooting ts;

	// Use this for initialization
	void Start () {
        ts = GetComponent<Complete.TankShooting>();
        audioSource = GetComponent<AudioSource>();
        InvokeRepeating("FireSomething", delay, fireRate);
    }

    void FireSomething()
    {
        //Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
        ts.Fire();
        audioSource.Play();

    }
}
