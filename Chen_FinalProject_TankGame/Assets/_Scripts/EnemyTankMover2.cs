﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTankMover : MonoBehaviour {

    private Rigidbody rb;
    private GameObject enemy;
    public float speed = 100f;


    // Use this for initialization
    void Start () 
    {
        rb = GetComponent<Rigidbody>();
        enemy = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (enemy == null)
            return;

        Vector3 direction = enemy.transform.position - transform.position;//targPos is a Vector3 that the tank is trying to go to
        if (direction.magnitude > 4f)
        {
            direction = direction.normalized;//Set magnitude of direction down to 1
            //rb.velocity += direction * speed * Time.deltaTime;//Speed is a float
            rb.MovePosition(rb.position + direction * speed * Time.deltaTime);
        }

        rb.MoveRotation(Quaternion.LookRotation(direction, Vector3.up));
    }
}
